import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'PinguinOS',
    Svg: require('@site/static/img/hacker-amico.svg').default,
    description: (
      <>
        PinguinOS est un jeu qui plonge les joueurs au cœur de l'action dans le domaine captivant de la cybersécurité.
        
      </>
    ),
  },
  {
    title: 'Expérience en cybersécurité',
    Svg: require('@site/static/img/security-amico.svg').default,
    description: (
      <>
        Préparez-vous à une expérience immersive unique où l'attaque et la défense se mêlent dans un environnement multijoueur dynamique.
      </>
    ),
  },
  {
    title: 'Synopsis',
    Svg: require('@site/static/img/endpoint-amico.svg').default,
    description: (
      <>
        PinguinOS vous plonge dans un monde virtuel où vous incarnez un maître de la cybersécurité, 
        chargé de défendre votre système contre des attaques malveillantes tout en tentant d'infiltrer et de déjouer les défenses adverses.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
